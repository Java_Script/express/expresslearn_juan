const express = require('express')
const router = express.Router()
const responseController = require('../controllers/response')

router.get('/', responseController.getResponse)

module.exports = router
